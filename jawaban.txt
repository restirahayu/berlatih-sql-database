JAWABAN

1. Buat database
   CREATE DATABASE myshop;

2. Buat tabel
users
MariaDB [myshop]> CREATE TABLE users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
Query OK, 0 rows affected (0.535 sec)

categories
MariaDB [myshop]> CREATE TABLE categories(
    -> id int primary key auto_increment,
    -> name varchar(255));
Query OK, 0 rows affected (0.735 sec)

items
MariaDB [myshop]> CREATE TABLE items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    ->  stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id)
    -> );
Query OK, 0 rows affected (0.491 sec)

3. Memasukan data pada tabel

users
MariaDB [myshop]> INSERT INTO users values ('','John Doe','john@doe.com','john123');

MariaDB [myshop]> INSERT INTO users values ('','Jane Doe','jane@doe.com','jenita123');

categories
MariaDB [myshop]> INSERT INTO categories values('','gadget');

MariaDB [myshop]> INSERT INTO categories values('','cloth');

MariaDB [myshop]> INSERT INTO categories values('','men');

MariaDB [myshop]> INSERT INTO categories values('','women');

MariaDB [myshop]> INSERT INTO categories values('','branded');


items
MariaDB [myshop]> INSERT INTO items values ('','Sumsang b50','hape keren dari merek sumsang',4000000,100,1);

MariaDB [myshop]> INSERT INTO items values ('','Uniklooh','baju keren dari brand ternama',500000,50,2);

MariaDB [myshop]> INSERT INTO items values ('','IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);


4. Mengambil data dari database

a. Mengambil data users
MariaDB [myshop]> SELECT id, name, email FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

b. Mengambil data items
Membuat sebuah query pada tabel items yang memiliki harga >1000000
MariaDB [myshop]> SELECT * FROM items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+

Membuat sebuah query  pada table items yang memiliki name serupa  �uniklo�, �watch�, atau �sang� (pilih salah satu saja).
MariaDB [myshop]> SELECT * FROM items WHERE name LIKE '%uniklo%';
+----+----------+-------------------------------+--------+-------+-------------+
| id | name     | description                   | price  | stock | category_id |
+----+----------+-------------------------------+--------+-------+-------------+
|  2 | Uniklooh | baju keren dari brand ternama | 500000 |    50 |           2 |
+----+----------+-------------------------------+--------+-------+-------------+

c. Mengambil data categories
MariaDB [myshop]> SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items JOIN categories ON items.category_id=categories.id;
+-------------+-----------------------------------+---------+-------+-------------+----------+
| name        | description                       | price   | stock | category_id | kategori |
+-------------+-----------------------------------+---------+-------+-------------+----------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+-------------+-----------------------------------+---------+-------+-------------+----------+

5. Mengubah data dari Database
MariaDB [myshop]> UPDATE items set price= 2500000 where name='Sumsang b50';
MariaDB [myshop]> SELECT * FROM items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
 